<?php 

namespace App\Services\Api;

use App\Services\Api\InterfaceReceiver;

class Receiver implements InterfaceReceiver
{
    const URL = 'https://jsonplaceholder.typicode.com/todos/1';

    public function process() //: ?array
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, self::URL);
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result , true);
    }

}   