<?php

namespace App\Services\Api;

use App\Services\Api\InterfaceValidator;
use App\Services\Api\ValidateEception;

class Validator implements InterfaceValidator
{
    public function validate($data) : void
    {
        if (empty($data)) {
            throw new ValidateException('Not found data');
        }

        if (!$data['id']) {
            throw new ValidateException('Not found id in data');
        }

        if (!$data['title']) {
            throw new ValidateException('Not found title in data');
        }
    }
}