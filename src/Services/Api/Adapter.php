<?php

namespace App\Services\Api;

use App\Services\Api\Sanitizer;
use App\Services\Api\Validator;
use App\Services\Api\Receiver;
use App\Services\Api\InterfaceSanitizer;
use App\Services\Api\InterfaceValidator;
use App\Services\Api\InterfaceReceiver;


class Adapter
{

    protected $sanitizer;

    protected $validator;

    protected $receiver;

    public function __construct(
        ?InterfaceReceiver $receiver,
        ?InterfaceValidator $validator,
        ?InterfaceSanitizer $sanitizer
    ) {
        $this->receiver = new Receiver ?? $receiver;
        $this->validator = new Validator ?? $validator;
        $this->sanitizer = new Sanitizer ?? $sanitizer;
    }

    public function getData() : array
    {        
        $data = $this->receiver->process();
        $this->validator->validate($data);
        $data = $this->sanitizer->sanitize($data);

        return $data;
    }
}