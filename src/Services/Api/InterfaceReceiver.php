<?php 

namespace App\Services\Api;

interface InterfaceReceiver
{
    public function process();
}