<?php 

namespace App\Services\Api;

interface InterfaceSanitizer
{
    public function sanitize($data) : array;
}