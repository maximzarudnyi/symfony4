<?php 

namespace App\Services\Api;

interface InterfaceValidator
{
    /**
     * @throws ValidateException
     * @param $data
     * @return void
     */
    public function validate($data) : void;
}