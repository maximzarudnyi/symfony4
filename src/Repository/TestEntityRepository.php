<?php

namespace App\Repository;

use App\Entity\TestEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * @method TestEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method TestEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method TestEntity[]    findAll()
 * @method TestEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TestEntityRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TestEntity::class);
    }

    /**
     * @param $value
     * @return TestEntity
     */
    public function finRandomField()
    {
        $random = mt_rand() / mt_getrandmax();

        $query = $this->getEntityManager()->createQuery(
                '                           
                    SELECT r 
                    FROM
                       App\Entity\TestEntity r 
                    WHERE
                       r.id >= 
                       (
                          SELECT
                             MAX(r1.id) * :rand 
                          FROM
                             App\Entity\TestEntity r1 
                       )
                    ORDER BY r.id
                '
            )
            ->setParameter('rand', $random)
            ->setMaxResults(1)
            ->getOneOrNullResult();

        return $query;
    }


    // /**
    //  * @return TestEntity[] Returns an array of TestEntity objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TestEntity
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
